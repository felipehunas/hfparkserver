from django.apps import AppConfig


class HfparkserverConfig(AppConfig):
    name = 'HFParkServer'
