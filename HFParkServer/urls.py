from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('parks/', csrf_exempt(views.ParksView.as_view()), name='parks-list'),
    path('parks/<int:id>', csrf_exempt(views.ParkDetailView.as_view()), name='park-detail'),
    path('parks/<int:id>/lots/', csrf_exempt(views.ParkLotsView.as_view()), name='park-lots')

]