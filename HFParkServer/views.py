import json
import datetime
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.views.generic import View, ListView, DetailView
from HFParkServer.models import Park, ParkingLot
from django.core.exceptions import ObjectDoesNotExist



class ParksView(ListView):
    model = Park

    def get(self, request, *args, **kwargs):
        all_as_list = list(self.get_queryset().all().values())
        return JsonResponse(all_as_list, safe=False)

    def post(self, request, *args, **kwargs):
        print(request.body)
        data = json.loads(request.body)
        try:
            park = Park.objects.get(pk=data['id'])
        except ObjectDoesNotExist:
            park = Park()

        park.name = data['name']
        park.att_date = datetime.datetime.now()
        park.total_free_lots = data['total_free_lots']
        park.total_lots = data['total_lots']
        park.save()
        data["id"] = park.id
        print(park)
        return JsonResponse(data, safe=False)


class ParkDetailView(View):

    def get(self, request, *args, **kwargs):
        park = Park.objects.filter(id=self.kwargs['id'])
        return JsonResponse({
            'park': park.values().first(),
            'lots': list(park.first().lots.values('status',
                                                  'position_00', 'position_01',
                                                  'position_10', 'position_11',
                                                  'position_20', 'position_21',
                                                  'position_30', 'position_31'))
        }, safe=False)


class ParkLotsView(ListView):
    model = ParkingLot

    def get_queryset(self):
        self.park = get_object_or_404(Park, id=self.kwargs['id'])
        return ParkingLot.objects.filter(park=self.park)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['park'] = self.park
        return context

    def get(self, request, *args, **kwargs):
        all_as_list = list(self.get_queryset().all().values())
        return JsonResponse(all_as_list, safe=False)

    def post(self, request, *args, **kwargs):
        print(request.body)
        data = json.loads(request.body)
        for lot in data['lots']:
            parkLot = ParkingLot()
            parkLot.park = Park.objects.get(pk=lot['parkId'])
            parkLot.id = lot['id']
            parkLot.status = lot['status']
            parkLot.position_00 = lot['position_00']
            parkLot.position_01 = lot['position_01']
            parkLot.position_10 = lot['position_10']
            parkLot.position_11 = lot['position_11']
            parkLot.position_20 = lot['position_20']
            parkLot.position_21 = lot['position_21']
            parkLot.position_30 = lot['position_30']
            parkLot.position_31 = lot['position_31']
            parkLot.save()
        return JsonResponse(data, safe=False)


# class ParksView(View):
#
#     def get(self, request, *args, **kwargs):
#         return JsonResponse([{'name':'suck'}], safe=False)
#
#     def post(self, request, *args, **kwargs):
#         return JsonResponse([{'you':'suck'}])


def index(request):
    return HttpResponse("Hello world, you are at the index of your application")
