from django.db import models


# Create your models here.
class Park(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    att_date = models.DateTimeField('last updated')
    total_lots = models.IntegerField()
    total_free_lots = models.IntegerField()

    def __str__(self):
        return self.name


class ParkingLot(models.Model):
    class Meta:
        unique_together = (('id', 'park'),)
    id = models.IntegerField(primary_key=True)
    park = models.ForeignKey(Park, on_delete=models.CASCADE, related_name='lots')
    status = models.TextField()
    position_00 = models.IntegerField(default=0)
    position_01 = models.IntegerField(default=0)
    position_10 = models.IntegerField(default=0)
    position_11 = models.IntegerField(default=0)
    position_20 = models.IntegerField(default=0)
    position_21 = models.IntegerField(default=0)
    position_30 = models.IntegerField(default=0)
    position_31 = models.IntegerField(default=0)

    def __str__(self):
        return self.desc_position()

    def desc_position(self):
        return f"Point 1: ({self.position_00},{self.position_01})"


class User(models.Model):
    id = models.IntegerField(default=200, name='id_usuario')
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=20)


class Place(models.Model):
    id = models.AutoField(primary_key=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
