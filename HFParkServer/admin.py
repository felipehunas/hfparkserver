from django.contrib import admin

# Register your models here.
from HFParkServer.models import Park, ParkingLot


class ParkAdmin(admin.ModelAdmin):
    fields = [
        'name',
        'att_date',
        'total_lots',
        'total_free_lots'
    ]


class ParkLotAdmin(admin.ModelAdmin):
    fields = [
        'park',
        'status',
        'position_00',
        'position_01',
        'position_10',
        'position_11',
        'position_20',
        'position_21',
        'position_30',
        'position_31'
    ]
    list_display = ('park', 'id', 'desc_position')


admin.site.register(Park, ParkAdmin)
admin.site.register(ParkingLot, ParkLotAdmin)
